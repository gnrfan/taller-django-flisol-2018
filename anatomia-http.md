# Anatomía HTTP

## Estructura de un solicitud (Request)

```
VERBO PATH HTTP/VERSION
Cabecera1: Valor 1
Cabecera2: Valor 2
Cabecera3: Valor 3
```

Ejemplo:

```
GET / HTTP/1.1
Host: www.google.com.pe
Accept: text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0
```

## Estructura de una respuesta (Response)

```
HTTP [CODIGO DE ESTADO]
Cabecera1: Valor 1
Cabecera2: Valor 2
Cabecera3: Valor 3
```

Ejemplo:

```
HTTP: 200 OK
Content-Type: text/html
Content-Length: 19

<h1>Hola mundo</h1>
```
