# Instalación de Django

## Paso 1: Instalar dependencias a nivel de paquetes de Ubuntu

```
$ sudo apt install build-essential python3-dev virtualenv python3-virtualenv python3-pip tree curl
```

## Paso 2: Crear un entorno virtual

```
$ virtualenv --no-site-packages --python=/usr/bin/python3 $HOME/env1
$ source $HOME/env1/bin/activate
```

## Paso 3: Instalar Django y sus dependencias a nivel de paquetes de Python

```
(env1)$ pip install Django
```
