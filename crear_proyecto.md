# Crear proyecto Django

## Crear un proyecto

```
$ django-admin.py startproject proyecto1
```

## Crear un app

```
$ django-admin.py startapp app1
```

## Crear tablas

```
$ cd proyecto1
$ python manage.py migrate
```

## Crear superusuario

```
$ python manage.py createsuperuser
```

## Ejecutar servidor web de desarrollo

```
$ python manage.py runserver 0.0.0.0:8000
```

Finalmente, dirigirse a: http://localhost:8000/admin/

