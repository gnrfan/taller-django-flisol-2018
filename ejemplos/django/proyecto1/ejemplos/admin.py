from django.contrib import admin
from .models import CategoriaEnlace
from .models import Enlace

class CategoriaEnlaceAdmin(admin.ModelAdmin):

    fields = (
        'id', 
        'nombre', 
        'slug', 
        'fecha_creacion',
        'fecha_modificacion'
    )
    
    list_display = (
        'id', 'nombre', 'slug', 'fecha_creacion'
    )
    
    readonly_fields = (
        'id', 'fecha_creacion', 'fecha_modificacion'    
    )
    
    search_fields = ('nombre', )


class EnlaceAdmin(admin.ModelAdmin):

    fields = (
        'id', 
        'categoria',
        'titulo', 
        'url', 
        'fecha_creacion',
        'fecha_modificacion'
    )
    
    list_display = (
        'id', 'titulo', 'categoria', 'fecha_creacion'
    )
    
    readonly_fields = (
        'id', 'fecha_creacion', 'fecha_modificacion'    
    )
    
    search_fields = ('titulo', 'url')


# Register your models here.

admin.site.register(
    CategoriaEnlace, CategoriaEnlaceAdmin
)

admin.site.register(Enlace, EnlaceAdmin)
