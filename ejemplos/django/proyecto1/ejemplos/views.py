import json
import datetime
from collections import OrderedDict
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Enlace

# Create your views here.

def hola(request):
    print("Corriendo...") # Se ve en la salida de runserver
    return HttpResponse("<h1>Hola</h1>")


def hola2(request, nombre):
    # nombre = nombre.lower().title()
    contexto = {'nombre': nombre} 
    return render(request, 'ejemplos/hola.html', contexto)

    
def hora(request):
    h = str(datetime.datetime.now()) 
    return HttpResponse("<h1>Hora actual: %s</h1>" % h)
    
    
def hora2(request):
    h = str(datetime.datetime.now())
    contexto = {'hora': h} 
    return render(request, 'ejemplos/hora.html', contexto)


def hora3(request):
    h = str(datetime.datetime.now()) 
    resp = HttpResponse("Hora actual: %s" % h)
    resp['Content-Type'] = 'text/plain'
    return resp


def suma(request, op1, op2):
    resultado = op1 + op2
    contexto = {
       'op1': op1,
       'op2': op2,
       'resultado': resultado,
       'operacion': 'suma'
    }
    return render(
        request, 
        'ejemplos/aritmetica.html', 
        contexto
    )
    
    
def aritmetica_json(request, op1, op2, operacion, nombre):
    resultado = operacion(op1, op2)
    doc = OrderedDict([
       ('nombre', nombre),
       ('op1', op1),
       ('op2', op2),
       ('resultado', resultado)
    ])
    resp = HttpResponse(json.dumps(doc, indent=4))
    resp['Content-Type'] = 'application/json'
    return resp


def aritmetica(request, op1, op2, operacion, nombre):
    resultado = operacion(op1, op2)
    contexto = {
       'op1': op1,
       'op2': op2,
       'resultado': resultado,
       'nombre': nombre
    }
    return render(
        request, 
        'ejemplos/aritmetica.html', 
        contexto
    )    


def redirigir(request, url):
    resp = HttpResponse()
    resp.status_code = 303
    resp['Location'] = url
    return resp

    
def redirigir2(request, url):
    return HttpResponseRedirect(url)
    
def enlaces(request):
    enlaces = Enlace.objects.all()
    contexto = {'enlaces': enlaces} 
    return render(
        request, 
        'ejemplos/enlaces.html', 
        contexto
    )
