from django.db import models

# Create your models here.

class CategoriaEnlace(models.Model):

    nombre = models.CharField(
        verbose_name='Nombre',
        max_length=64,
        unique=True
    )
    
    slug = models.SlugField(
        verbose_name='SLUG'
    )
    
    fecha_creacion = models.DateTimeField(
        verbose_name='Fecha y hora de creación',
        auto_now_add=True
    )
    
    fecha_modificacion = models.DateTimeField(
        verbose_name='Fecha y hora de modificación',
        auto_now=True
    )
    
    class Meta:
        verbose_name = 'categoría de enlace'
        verbose_name_plural = 'categorías de enlace'

    def __str__(self):
        return self.nombre
        

class Enlace(models.Model):

    categoria = models.ForeignKey(CategoriaEnlace,
        verbose_name='Categoría',
        on_delete=models.PROTECT
    )

    titulo = models.CharField(
        verbose_name='Título',
        max_length=255
    )
    
    url = models.URLField(
        verbose_name='URL'
    )
    
    fecha_creacion = models.DateTimeField(
        verbose_name='Fecha y hora de creación',
        auto_now_add=True
    )
    
    fecha_modificacion = models.DateTimeField(
        verbose_name='Fecha y hora de modificación',
        auto_now=True
    )
    
    class Meta:
        verbose_name = 'enlace'
        verbose_name_plural = 'enlaces'

    def __str__(self):
        return self.titulo
