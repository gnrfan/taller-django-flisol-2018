# Generated by Django 2.0.2 on 2018-03-25 02:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ejemplos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Enlace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=255, verbose_name='Título')),
                ('url', models.URLField(verbose_name='URL')),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True, verbose_name='Fecha y hora de creación')),
                ('fecha_modificacion', models.DateTimeField(auto_now=True, verbose_name='Fecha y hora de modificación')),
            ],
            options={
                'verbose_name': 'enlace',
                'verbose_name_plural': 'enlaces',
            },
        ),
        migrations.AlterModelOptions(
            name='categoriaenlace',
            options={'verbose_name': 'categoría de enlace', 'verbose_name_plural': 'categorías de enlace'},
        ),
        migrations.AddField(
            model_name='enlace',
            name='categoria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ejemplos.CategoriaEnlace', verbose_name='Categoría'),
        ),
    ]
