"""proyecto1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import operator
from django.contrib import admin
from django.urls import path
from ejemplos import views as ejemplos_views

urlpatterns = [
    path('hola/', ejemplos_views.hola, name='hola'),
    path('hola/<str:nombre>/', ejemplos_views.hola2, name='hola2'),
    path('aritmetica/suma/<int:op1>/mas/<int:op2>/', ejemplos_views.suma, name='suma'),
    path('aritmetica/resta/<int:op1>/menos/<int:op2>/', ejemplos_views.aritmetica, name='resta', kwargs={'nombre': 'resta', 'operacion': operator.sub}),
    path('aritmetica/multiplicacion/<int:op1>/por/<int:op2>/', ejemplos_views.aritmetica, name='multiplicacion', kwargs={'nombre': 'multiplicación', 'operacion': operator.mul}),
    path('aritmetica/division/<int:op1>/entre/<int:op2>/', ejemplos_views.aritmetica_json, name='division', kwargs={'nombre': 'división', 'operacion': operator.floordiv}),
    path('hora/', ejemplos_views.hora, name='hora'),
    path('hora2/', ejemplos_views.hora2, name='hora2'),
    path('hora.txt', ejemplos_views.hora3, name='hora3'),
    path('ir-a-google/', ejemplos_views.redirigir2, name='redireccion_google', kwargs={'url': 'http://www.google.com.pe'}),
     path('enlaces/', ejemplos_views.enlaces, name='enlaces'),
    path('admin/', admin.site.urls),
]
