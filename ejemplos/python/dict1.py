#!/usr/bin/env python3
# -*- coding: utf8 -*-

colores = [
    ("red", "rojo"),
    ("blue", "azul"),
    ("green", "verde")
]

llave = "green"
traduccion = None
for color_ing, color_esp in colores:
    if llave == color_ing:
        traduccion = color_esp
print(traduccion)

colores = {
    "red": "rojo",
    "blue": "azul",
    "green": "verde",
}

traduccion = colores[llave]
print(traduccion)

# La llave no se puede repetir porque redefine el valor asociado

colores = {
    "red": "rojo",
    "blue": "azul",
    "green": "verde",
    "red": "colorado"
}

llave = "red"
traduccion = colores[llave]
print(traduccion)

