#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""

Generar 1000 placas distintas:

AP-1234
AQP-321
A9P-123
A9-PZ5-4821

L = solo letras
N = solo numeros
A = letras + numeros

plantillas = ['LL-NNNN', 'LLL-NNN', 'LNL-NNN', 'LA-AA-NNNN']
"""
