#!/usr/bin/env python3
# -*- coding: utf-8 -*-
    
def invocar_dos_veces(f):
    """
    Un decorador neutro, que no agrega ni modifica
    la funcionalidad original.
    """
    def aux(*args, **kwargs):
        valor1 = f(*args, **kwargs)
        valor2 = f(*args, **kwargs)
        return (valor1, valor2)
    return aux

@invocar_dos_veces 
@invocar_dos_veces   
@invocar_dos_veces
def subrayar(titulo, caracter='-'):
    print(titulo)
    print(caracter*len(titulo))


if __name__ == '__main__':

    subrayar("Hola mundo")    

