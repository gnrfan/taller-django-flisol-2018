#!/usr/bin/env python3
# -*- coding: utf8 -*-

import time
import resource

INICIO = 1
FIN = 100
PAUSA = 1

i = INICIO
lista = []

while i <= FIN:
    lista.append(i)
    print(i)
    print(lista)
    # Detecta la cantidad de memoria usada por el programa en bytes
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    print(mem)
    time.sleep(PAUSA)
