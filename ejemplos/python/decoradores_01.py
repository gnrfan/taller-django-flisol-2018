#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def crear_sumadora(n):
    def aux(x):
        return x + n
    return aux
    
def decorador(f):
    """ 
    Un decorador neutro, que no agrega ni modifica
    la funcionalidad original.
    """
    def aux(*args, **kwargs):
        return f(*args, **kwargs)
    return aux
    

if __name__ == '__main__':

    crear_sumadora_2 = decorador(crear_sumadora)
    sumadora_de_cincos = crear_sumadora_2(5)
    print(sumadora_de_cincos(10))


