#!/usr/bin/env python3
# -*- coding: utf8 -*-

from suma2 import suma
from subrayar import subrayar

if __name__ == '__main__':
    
    # Invocaciones "hardcodeadas" ó "codificadas en duro"
    print(suma(1, 1, 1, 1, 1))
    print(suma(a=1, b=1, x=1, y=1, z=1))
    print(suma(1, 1, 1, y=1, z=1))
    
    # Invocación programática
    subrayar("Invocación programática", nl=True)
    args = (1, 1, 1, 1, 1)
    print(suma(*args)) # suma(1, 1, 1, 1, 1)
    
    kwargs = {'a': 10, 'b': 20, 'x': 500, 'y': 1000}
    print(suma(**kwargs)) # suma(a=10, b=20, x=500, y=100)
    
    # Argumentos dinámicos
    try:
        a = int(input("a: "))
    except ValueError:
        exit(1)
 
    try:
        b = int(input("b: "))
    except ValueError:
        exit(1)

    print(suma(a, b))
