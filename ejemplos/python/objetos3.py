#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Sumadora(object):

    def __init__(self, sumando):
        self.sumando = sumando
        
    def __call__(self, x):
        return x + self.sumando
        
if __name__ == '__main__':

    sumadora_de_cincos = Sumadora(5)
    print(sumadora_de_cincos(10))
    
    sumadora_de_cincos.sumando = 10
    print(sumadora_de_cincos(10))
    
