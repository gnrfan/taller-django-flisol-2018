#!/usr/bin/env python3
# -*- coding: utf8 -*-
from random import randint

    
if __name__ == '__main__':

    coordenadas = []
    for i in range(10):
        latitud = (1 if randint(0, 1) == 0 else -1) * randint(0, 90)
        longitud = (1 if randint(0, 1) == 0 else -1) * randint(0, 180)
        punto = (latitud, longitud)
        coordenadas.append(punto)
    print(coordenadas)
    
    nuevas_coordenadas = [
        (lat+5,lng+5) for lat,lng in coordenadas
    ]
    
    print(nuevas_coordenadas)
    
    nuevas_coordenadas_2 = [
        {'a': lat, 'b': lng} for lat, lng in nuevas_coordenadas
    ]
    
    print(nuevas_coordenadas_2)     



