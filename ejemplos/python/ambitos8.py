#!/usr/bin/env python3
# -*- coding: utf8 -*-

a = 100

def definir_imprimir_a(a):
    # Esta función es un closure porque encierra
    # una referencia a un símbolo que NO es ni parámetro
    # ni variable local.
    def imprimir_a():
        print(a)
    return imprimir_a
    
if __name__ == '__main__':
    imprimir_a = definir_imprimir_a(300)
    imprimir_a()
    a = 400
    imprimir_a = definir_imprimir_a(500)
    imprimir_a()

