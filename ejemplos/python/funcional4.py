#!/usr/bin/env python3
# -*- coding: utf8 -*-

from functools import reduce

if __name__ == '__main__':

    cadena = "Hola mundo"
    letras = list(map(lambda letra: letra.upper(), cadena))
    print(letras)
    print(reduce(lambda a,b: ''.join([a,b]), letras, ""))
