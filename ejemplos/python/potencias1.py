#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Imprimir las potencias de 2 desde 0 hasta 5.
"""

p = 1
e = 0

while e <= 5:
    print(p)
    p = p * 2
    e = e + 1
