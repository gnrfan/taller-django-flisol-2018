#!/usr/bin/env python3
# -*- coding: utf8 -*-

from suma2 import suma
from subrayar import subrayar

def invocar_funcion(funcion, *args, **kwargs):
    return funcion(*args, **kwargs)    

if __name__ == '__main__':

    print(invocar_funcion(suma, 1, 1, 1, 1, 1))
    print(invocar_funcion(suma, a=1, b=1, x=1, y=1, z=1))
    print(invocar_funcion(suma, 1, 1, 1, y=1, z=1))
    
    invocar_funcion(subrayar, "Hola mundo", "-", nl=True)
    print(invocar_funcion(lambda a,b: a - b, 100, 10))

