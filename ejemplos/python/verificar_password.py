#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Implementar una clase VerificadorPassword que reciba
como parámetros opcionales:

* longitud_minima (Por omisión 8)
* min_mayusculas (Por omisión 1)
* min_minusculas (Por omisión 1)
* min_numeros = (Por omision 1)
* min_simbolos = (Por omision 1)

Hacer del protocolo de callables (método __call_) para
verificar un password según los parámetros del verificador
devolviendo un valor booleano que indica si la contraseña
cumple o no con las políticas.

Ejemplo:

verificar = VerificadorPassword(
    longitud_minima=10,
    min_simbolos=2
)

print(verificar("Sup3r$3cr3t0"))

"""
