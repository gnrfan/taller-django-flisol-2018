#!/usr/bin/env python3
# -*- coding: utf8 -*-

a = 100

def definir_imprimir_a():
    a = 300
    def imprimir_a():
        print(a)
    return imprimir_a
    
if __name__ == '__main__':
    imprimir_a = definir_imprimir_a()
    imprimir_a()
    a = 400
    imprimir_a()

