#!/usr/bin/env python3
# -*- coding: utf8 -*-

from suma2 import suma

def invocar_suma(*args, **kwargs):
    return suma(*args, **kwargs)    

if __name__ == '__main__':

    print(invocar_suma(1, 1, 1, 1, 1))
    print(invocar_suma(a=1, b=1, x=1, y=1, z=1))
    print(invocar_suma(1, 1, 1, y=1, z=1))

