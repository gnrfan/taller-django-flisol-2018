#!/usr/bin/env python3
# -*- coding: utf8 -*-

import string
from random import choice

NUM_LICENCIAS = 10
NUM_SEGMENTOS = 5
CARACTERES_POR_SEGMENTO = 5
ALFABETO = string.ascii_uppercase + string.digits

licencias = []

while len(licencias) < NUM_LICENCIAS:

    licencia = None
    segmentos = []
     
    for s in range(NUM_SEGMENTOS):
         segmento = []
         for x in range(CARACTERES_POR_SEGMENTO):
             segmento.append(choice(ALFABETO))
         segmentos.append(''.join(segmento))
    licencia = '-'.join(segmentos)

    # Solo agregarla si no esta repetida
    if licencia not in licencias:
        licencias.append(licencia)
        
print("\n".join(licencias))
