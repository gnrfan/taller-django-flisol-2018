#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

class ListaRara(object):

    def __init__(self, max_items=100):
        self.max_items = max_items
        
    def __len__(self):
        return randint(1, self.max_items)
        
if __name__ == '__main__':

    lista_rara = ListaRara()
    print("# de Items: %d" % len(lista_rara))
