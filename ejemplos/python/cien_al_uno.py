#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Imprimir los números del 1 al 100.
"""

VALOR_INICIAL = 100
VALOR_FINAL = 1
DECREMENTO = 1

i = VALOR_INICIAL

while i >= VALOR_FINAL:
    print(i)
    i -= DECREMENTO
