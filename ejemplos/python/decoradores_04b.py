#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

def verificar_mayoria_edad(edad_minima=18):
    def decorador(f):
        def aux(edad, *args, **kwargs):
            if edad >= edad_minima:
                return f(edad, *args, **kwargs)
            else:
                print("ERROR: Ud. no es mayor de edad.")
                print("Edad mínima: %d" % edad_minima)
        return aux
    return decorador    

@verificar_mayoria_edad(21)
def ver_pelicula(edad):
    print("Tengo %d años y estoy viendo la película" % edad)

@verificar_mayoria_edad(16)
def votar(edad):
    print("Tengo %d años y estoy votando" % edad)

def comprar_licor(edad):
    print("Tengo %d años y estoy comprando licor" % edad)


if __name__ == '__main__':
    
    edad = randint(10, 60)
    print("edad: %d" % edad)
    ver_pelicula(edad)
    votar(edad)
    
    #mayoria_21 = verificar_mayoria_edad(21)
    #comprar_licor = mayoria_21(comprar_licor)
     
    comprar_licor = verificar_mayoria_edad(21)(comprar_licor)
    comprar_licor(edad)   

