#!/usr/bin/env python3
# -*- coding: utf8 -*-

import os
import resource
import subprocess
import time

INICIO = 1
FIN = 100
PAUSA = 1

i = INICIO
lista = []

def memory_usage_ps():
    out = subprocess.Popen(['ps', 'v', '-p', str(os.getpid())],
    stdout=subprocess.PIPE).communicate()[0].split(b'\n')
    vsz_index = out[0].split().index(b'RSS')
    mem = float(out[1].split()[vsz_index]) / 1024
    return mem

while i <= FIN:
    lista.append(i)
    print(i)
    print(lista)
    # Detecta la cantidad de memoria usada por el programa en bytes
    mem = memory_usage_ps()
    print(mem)
    time.sleep(PAUSA)
