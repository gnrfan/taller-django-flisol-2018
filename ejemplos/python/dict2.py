#!/usr/bin/env python3
# -*- coding: utf8 -*-

paises = {
    "Peru": "Lima",
    "Argentina": "Buenos Aires",
    "Chile": "Santiago",
    "Francia": "Paris",
    "Alemania": "Berlin",
    "Rusia": "Moscu",
    "Iraq": "Bagdak"
}

for llave, valor in paises.items():
    print(llave, valor)
