#!/usr/bin/env python3
# -*- coding: utf8 -*-

a = 10

def imprimir_a():
    global a
    a = 20
    print(a)
    
if __name__ == '__main__':
    imprimir_a()
    print(a)
