#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

def verificar_mayoria_edad(f):
    def aux(edad, *args, **kwargs):
        if edad >= 18:
            return f(edad, *args, **kwargs)
        else:
            print("ERROR: Ud. no es mayor de edad.")
    return aux    

@verificar_mayoria_edad
def ver_pelicula(edad):
    print("Tengo %d años y estoy viendo la película" % edad)

@verificar_mayoria_edad
def votar(edad):
    print("Tengo %d años y estoy votando" % edad)


if __name__ == '__main__':
    
    edad = randint(10, 60)
    print("edad: %d" % edad)
    ver_pelicula(edad)
    votar(edad)

