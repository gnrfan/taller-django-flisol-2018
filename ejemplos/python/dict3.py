#!/usr/bin/env python3
# -*- coding: utf8 -*-

from collections import OrderedDict

paises = OrderedDict([
    ("Peru", "Lima"),
    ("Argentina", "Buenos Aires"),
    ("Chile", "Santiago"),
    ("Francia", "Paris"),
    ("Alemania", "Berlin"),
    ("Rusia", "Moscu"),
    ("Iraq", "Bagdak")
])


paises["Estados Unidos"] = "Washington D.C."
paises["Japon"] = "Tokio"

for llave, valor in paises.items():
    print(llave, valor)
