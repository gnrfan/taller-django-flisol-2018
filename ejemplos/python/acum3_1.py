#!/usr/bin/env python3
# -*- coding: utf8 -*-

from random import randint

ALEATORIO_MIN = 1
ALEATORIO_MAX = 100
MAX_DIVISIBLES = 5
DIVISOR = 3

divisibles_por_tres = []
encontrados = False


while not encontrados:
    numero_aleatorio = randint(ALEATORIO_MIN, ALEATORIO_MAX)
    print(numero_aleatorio)
    if (numero_aleatorio % DIVISOR == 0) and \
        (numero_aleatorio not in divisibles_por_tres):
        divisibles_por_tres.append(numero_aleatorio)
    if len(divisibles_por_tres) == MAX_DIVISIBLES:
        encontrados = True
    
divisibles_por_tres.sort()
print(divisibles_por_tres)
