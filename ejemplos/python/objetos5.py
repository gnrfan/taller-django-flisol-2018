#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class CadenaIterable(object):

    def __init__(self, valor):
        self.valor = valor
        self.rewind()
        
    def __next__(self):
        if self.apuntador < len(self.valor):
            resultado = self.valor[self.apuntador]
            self.apuntador += 1
        else:
            raise StopIteration
        return resultado
            
    def __iter__(self):
        return self
        
    def rewind(self):
        if len(self.valor) > 0:
            self.apuntador = 0
        else:
            self.apuntador = None

if __name__ == '__main__':

    obj = CadenaIterable("Hola")
