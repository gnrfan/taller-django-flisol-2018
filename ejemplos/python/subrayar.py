#!/usr/bin/env python3
# -*- coding: utf8 -*-

def subrayar(cadena, caracter='=', nl=False):
    print(cadena)
    print(caracter*len(cadena))
    if nl:
        print()
       
if __name__ == '__main__': 
    subrayar("Hola mundo")
    subrayar("Hola mundo", '*')
    subrayar("Hola mundo", '-', True)


