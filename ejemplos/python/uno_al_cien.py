#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Imprimir los números del 1 al 100.
"""

i = 1

while i <= 100:
    print(i)
    i += 1
