#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

IGV = 0.18

class Producto(object):

    def __init__(self, descripcion, precio_unitario):
        self.descripcion = descripcion
        self.precio_unitario = precio_unitario
        

class Empresa(object):

    def __init__(self, razon, ruc, direccion):
        self.razon = razon
        self.ruc = ruc
        self.direccion = direccion


class Imprenta(Empresa):

    def __init__(self, num_autorizacion, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.num_autorizacion = num_autorizacion
        
        
class Serie(object):
    
    def __init__(
        self, num, inicio, fin, 
        empresa, imprenta, fecha_emision
    ):
        self.num = num
        self.inicio = inicio
        self.fin = fin
        self.empresa = empresa
        self.imprenta = imprenta
        self.fecha_emision = fecha_emision
        
    def formatear(self, correlativo):
        return '%03d - %07d' % (
            self.num,
            correlativo
        )
        
    def verificar_correlativo(self, correlativo):
        return self.inicio <= correlativo and \
            self.fin >= correlativo


class DetalleFactura(object):

    def __init__(self, producto, cantidad):
        self.producto = producto
        self.cantidad = cantidad
        
    def calcular_subtotal(self):
        return self.producto.precio_unitario * self.cantidad
        
        
class Factura(object):

    def __init__(
        self, emisor, receptor, 
        imprenta, serie, correlativo
    ):
        self.emisor = emisor
        self.receptor = receptor
        self.imprenta = imprenta
        self.serie = serie
        if serie.verificar_correlativo(correlativo):                        
            self.correlativo = correlativo
        else:
            msg  = 'El correlativo está fuera del '
            msg += 'rango de la serie.'
            raise RuntimeError(msg)
        self.resetear_detalles()
        
    def resetear_detalles(self):
        self.detalles = []
    
    def obtener_listado_productos(self):
        return [
            detalle.producto.descripcion
            for detalle in self.detalles
        ]
    
    def existe_producto(self, producto):
        productos = self.obtener_listado_productos()
        return producto.descripcion in productos
    
    def agregar_detalle(self, producto, cantidad):
        if self.existe_producto(producto):
            plantilla  = 'El producto \'%s\' ya ha sido '
            plantilla += 'agregado a los detalles'
            raise RuntimeError(plantilla % producto.descripcion)
        detalle = DetalleFactura(producto, cantidad)
        self.detalles.append(detalle)
        
    def calcular_subtotal(self):
        return sum(
            [
                detalle.calcular_subtotal() 
                for detalle in self.detalles 
            ]
        )

    def calcular_impuesto(self, subtotal=None):
        if subtotal is None:
            subtotal = self.calcular_subtotal()
        return subtotal * IGV
        
    def calcular_total(self, subtotal=None, impuesto=None):
        if subtotal is None:
            subtotal = self.calcular_subtotal()
        if impuesto is None:
            impuesto = self.calcular_impuesto(subtotal)
        return subtotal + impuesto
        
    def actualizar_totales(self):
        self.subtotal = self.calcular_subtotal()
        self.impuesto = self.calcular_impuesto(self.subtotal)
        self.total = self.calcular_total(
            self.subtotal, self.impuesto
        )
        
        
if __name__ == '__main__':
    
    emisor = Empresa(
        razon='Python S.A.C.',
        ruc='10102030405',
        direccion='Av. Carlos Izaguire 123'
    )
    
    receptor = Empresa(
        razon='Linux S.A.C.',
        ruc='10403020105',
        direccion='Av. Arequipa 567'
    )
    
    imprenta = Imprenta(
        num_autorizacion='0123456789',
        razon='Imprenta Django S.A.C.',
        ruc='10506070805',
        direccion='Av. Wilson 1024'
    )
    
    serie = Serie(
        num=1,
        inicio=1,
        fin=1000,
        empresa=emisor,
        imprenta=imprenta,
        fecha_emision=date(2018, 3, 3)
    )
    
    factura = Factura(
        emisor=emisor,
        receptor=receptor,
        imprenta=imprenta,
        serie=serie,
        correlativo=765
    )
    
    producto1 = Producto(
        descripcion='Chompa de dralon',
        precio_unitario=70.0
    )
    
    producto2 = Producto(
        descripcion='Chompa de orlon',
        precio_unitario=50.0
    )

    producto3 = Producto(
        descripcion='Chompa de lana de alpaca',
        precio_unitario=120.0
    )

    factura.agregar_detalle(producto1, 4)
    factura.agregar_detalle(producto2, 2)
    factura.agregar_detalle(producto3, 5)

    factura.actualizar_totales()
    print("Subtotal: S/. %.2f" % factura.subtotal)
    print("Impuesto: S/. %.2f" % factura.impuesto)
    print("Total: S/. %.2f" % factura.total)
