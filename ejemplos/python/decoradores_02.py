#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def subrayar(titulo, caracter='-'):
    print(titulo)
    print(caracter*len(titulo))
    
def invocar_dos_veces(f):
    """
    Un decorador neutro, que no agrega ni modifica
    la funcionalidad original.
    """
    def aux(*args, **kwargs):
        valor1 = f(*args, **kwargs)
        valor2 = f(*args, **kwargs)
        return (valor1, valor2)
    return aux
    

if __name__ == '__main__':

    subrayado_doble = invocar_dos_veces(subrayar)
    subrayado_doble("Hola mundo")
    
    subrayado_cuadruple = invocar_dos_veces(subrayado_doble)
    subrayado_cuadruple("Hola mundo")    

