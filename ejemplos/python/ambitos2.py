#!/usr/bin/env python3
# -*- coding: utf8 -*-

a = 10

def imprimir_a():
    a = 20 # Esta "a" es un variable local que tapa a la "a" global
    print(a)
    
if __name__ == '__main__':
    imprimir_a()
    print(a)
