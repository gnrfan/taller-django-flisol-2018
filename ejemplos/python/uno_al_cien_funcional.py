#!/usr/bin/env python3
# -*- coding: utf8 -*-

MAXIMO = 100

def iterar(x):
    print(x)
    if x == MAXIMO:
        return
    else:
        return iterar(x+1)
        
if __name__ == '__main__':
    iterar(1)
