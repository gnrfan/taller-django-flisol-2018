#!/usr/bin/env python3
# -*- coding: utf8 -*-

from random import randint
from time import sleep

RANDOM_MIN = 1
RANDOM_MAX = 6
ACUM_MIN = 100
ELEMENTO_NEUTRO = 0
PAUSA = 0.25

acum = ELEMENTO_NEUTRO

vueltas = 0
while acum < ACUM_MIN:
    vueltas += 1
    numero_aleatorio = randint(RANDOM_MIN, RANDOM_MAX)
    acum += numero_aleatorio
    print("numero aleatorio: %d - acumulado: %d - vuelta: %d" % \
        (numero_aleatorio, acum, vueltas))
    sleep(PAUSA)

print("acumulador: %d"  % acum)
print("vueltas: %d" % vueltas)
