#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""

Imprimir la siguiente secuencia de asteriscos:

*
**
***
****
*****

"""

veces = 1

print("*" * veces)
veces = veces + 1

print("*" * veces)
veces = veces + 1

print("*" * veces)
veces = veces + 1

print("*" * veces)
veces = veces + 1

print("*" * veces)
veces = veces + 1

print("veces: {}".format(veces))
