#!/usr/bin/env python3
# -*- coding: utf8 -*-

numeros = list(range(1, 10 + 1))
print(sum([x*x for x in numeros if x*x % 2 == 0]))
