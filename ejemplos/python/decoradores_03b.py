#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint
        
def invocacion_aleatoria(f):
    """
    Un decorador neutro, que no agrega ni modifica
    la funcionalidad original.
    """
    def aux(*args, **kwargs):
        if randint(0, 1) == 1:
            return f(*args, **kwargs)
    return aux
    
@invocacion_aleatoria
def sumar(a, b):
    return a + b


if __name__ == '__main__':
    print(sumar(1,1))
