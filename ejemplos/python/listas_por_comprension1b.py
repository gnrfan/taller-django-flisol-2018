#!/usr/bin/env python3
# -*- coding: utf8 -*-

print(sum([x*x for x in range(1, 10 + 1) if x*x % 2 == 0]))
