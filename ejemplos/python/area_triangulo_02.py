#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

print("Área del triángulo")
print("==================")
print()

base = float(input("Ingrese la base: "))
altura = float(input("Ingrese la altura: "))

area = (base * altura) / 2.0

print("Base: %.2f" % base)
print("Altura: %.2f" % altura)
print("Area: %.2f" % area)
