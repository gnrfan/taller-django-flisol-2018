#!/usr/bin/env python3
# -*- coding: utf8 -*-

from suma2 import suma

def crear_operacion(operador):

    def multiplicacion(a, b):
        return a * b

    if operador == '+':
        return suma
    elif operador == '-':
        return lambda a, b: a - b
    elif operador == '*':
        return multiplicacion

if __name__ == '__main__':

    adicion = crear_operacion('+')
    print(adicion(1,1))
    substraccion = crear_operacion('-')
    print(substraccion(100, 10))
    multiplicar = crear_operacion('*')
    print(multiplicar(10,10))
