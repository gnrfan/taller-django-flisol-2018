#!/usr/bin/env python3
# -*- coding: utf8 -*-

INICIO = 1
FINAL = 100

acum = 0

for i in range(INICIO, FINAL + 1):
    if i % 2 != 0:
        acum += i

print(acum)
