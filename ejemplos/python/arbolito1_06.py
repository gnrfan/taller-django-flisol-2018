#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""

Imprimir la siguiente secuencia de asteriscos:

*
**
***
****
*****

"""

ASTERISCOS_INICIAL = 1
ASTERISCOS_INCREMENTO = 1
MAX_ASTERISCOS = 15

veces = ASTERISCOS_INICIAL

while veces <= MAX_ASTERISCOS:
    print("*" * veces)
    veces = veces + ASTERISCOS_INCREMENTO

print("veces: {}".format(veces))
