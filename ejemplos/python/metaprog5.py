#!/usr/bin/env python3
# -*- coding: utf8 -*-

funciones = {}

def crear_sumadora(n):
    def sumadora(x):
        return x + n
    return sumadora

if __name__ == '__main__':
    
    for n in range(1, 100 + 1):
        nombre = "sumar_%d" % n
        funciones[nombre] = crear_sumadora(n)
    
    for n in range(1, 100 + 1):
        nombre = "sumar_%d" % n
        print(nombre)
        print(funciones[nombre](0))
