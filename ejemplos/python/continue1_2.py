#!/usr/bin/env python3
# -*- coding: utf8 -*-

i = 1
while i <= 100:
    if i % 2 != 0:
        continue
    print(i)
    i += 1
