#!/usr/bin/env python3
# -*- coding: utf8 -*-

nombres = input("Nombres: ")
lista_nombres = nombres.split(',')
contadores = {}


for indice, nombre in enumerate(lista_nombres):
    lista_nombres[indice] = nombre.strip()

for nombre in lista_nombres:
    if nombre in contadores:
        contadores[nombre] += 1
    else:
        contadores[nombre] = 1

print(contadores)
