#!/usr/bin/env python3
# -*- coding: utf8 -*-

a = 10

def imprimir_a():
    a = 20
    b = 100
    print(a)
    print(b)
    
if __name__ == '__main__':
    imprimir_a()
    print(a)
    print(b)
