#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""

Imprimir la siguiente secuencia de asteriscos:

*
**
***
****
*****

"""

print("*" * 1)
print("*" * 2)
print("*" * 3)
print("*" * 4)
print("*" * 5)
