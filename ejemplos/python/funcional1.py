#!/usr/bin/env python3
# -*- coding: utf8 -*-

def mapear(lista, funcion):
    nueva_lista = []
    for valor in lista:
        nueva_lista.append(funcion(valor))
    return nueva_lista
    
    
#def cuadrado(x):
#    return x * x
    
if __name__ == '__main__':

    numeros = list(range(1, 10 + 1))
    print(numeros)
    cuadrados = mapear(numeros, lambda x: x*x)
    print(cuadrados)
