#!/usr/bin/env python3
# -*- coding: utf8 -*-

from random import randint
from time import sleep

RANDOM_MIN = 1
RANDOM_MAX = 6
ACUM_MIN = 100
ELEMENTO_NEUTRO = 0
PAUSA = 0.5

acum = ELEMENTO_NEUTRO

while acum < ACUM_MIN:
    numero_aleatorio = randint(RANDOM_MIN, RANDOM_MAX)
    print("numero aleatorio: %d - acumulado: %d" % (numero_aleatorio, acum))
    acum += numero_aleatorio
    sleep(PAUSA)

print(acum)
