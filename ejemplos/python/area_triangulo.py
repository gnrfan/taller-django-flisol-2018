#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

base = 25.34
altura = 42.18

area = (base * altura) / 2.0

print("Área del triángulo")
print("==================")
print()

print("Base: %.2f" % base)
print("Altura: %.2f" % altura)
print("Area: %.2f" % area)
