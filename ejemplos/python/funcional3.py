#!/usr/bin/env python3
# -*- coding: utf8 -*-

from functools import reduce

if __name__ == '__main__':

    numeros = list(range(1, 10 + 1))
    print(numeros)
    cuadrados = list(map(lambda x: x*x, numeros))
    print(cuadrados)
    cuadrados_pares = list(filter(lambda x: x % 2 == 0, cuadrados))
    print(cuadrados_pares)
    print(reduce(lambda x,y: x + y, cuadrados_pares, 0))
