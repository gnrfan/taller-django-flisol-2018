#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Rectangulo(object):

    def __init__(self, base, altura):
        self.base = base
        self.altura = altura
    
    def calcular_area(self):
        return self.base * self.altura
        
    def calcular_perimetro(self):
        return self.base * 2 + self.altura * 2
        
if __name__ == '__main__':

    obj1 = Rectangulo(base=100, altura=200)
    print("Area: %.2f" % obj1.calcular_area())
    print("Perímetro: %.2f" % obj1.calcular_perimetro())
