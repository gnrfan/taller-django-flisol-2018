#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Rectangulo(object):

    def __init__(self, base, altura):
        self.base = base
        self.altura = altura
    
    def calcular_area(self):
        return self.base * self.altura
        
    def calcular_perimetro(self):
        return self.base * 2 + self.altura * 2
        
    def __str__(self):
        return "<Rectangulo base=%.2f altura=%.2f>" % (
            self.base,
            self.altura
        )
        
    def __repr__(self):
        return str(self)


class Cuadrado(Rectangulo):
    
    def __init__(self, lado):
        super().__init__(base=lado, altura=lado)
        self.lado = lado
    
    
        
if __name__ == '__main__':

    obj1 = Rectangulo(base=100, altura=200)
    print("Area: %.2f" % obj1.calcular_area())
    print("Perímetro: %.2f" % obj1.calcular_perimetro())
    
    obj2 = Cuadrado(lado=100)
    print("Area: %.2f" % obj2.calcular_area())
    print("Perímetro: %.2f" % obj2.calcular_perimetro())
    
