#!/usr/bin/env python3
# -*- coding: utf8 -*-

nombres = input("Nombres: ").split(",")
contadores = {}

for nombre in nombres:
    nombre = nombre.strip()
    if nombre not in contadores:
        contadores[nombre] = 0
    contadores[nombre] += 1

print(contadores)
