#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Imprimir las potencias de 2 desde 0 hasta 5.
"""

BASE = 2
EXPONENTE_INICIAL = 0
EXPONENTE_FINAL = 5
INCREMENTO_EXPONENTE = 1
ELEMENTO_NEUTRO_MULTIPLICACION = 1

p = ELEMENTO_NEUTRO_MULTIPLICACION
e = EXPONENTE_INICIAL

while e <= EXPONENTE_FINAL:
    print("%d elevado al exponente %d = %d" % (BASE, e, p))
    p *= BASE #p = p * BASE
    e += INCREMENTO_EXPONENTE
