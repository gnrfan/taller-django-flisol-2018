#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Imprimir los números del 1 al 100.
"""

VALOR_INICIAL = 1
VALOR_FINAL = 100
INCREMENTO = 1
DIVISOR = 2

i = VALOR_INICIAL

while i <= VALOR_FINAL:
    if i % DIVISOR == 0:
        print(i)  
    i += INCREMENTO
