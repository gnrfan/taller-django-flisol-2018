#!/usr/bin/env python3
# -*- coding: utf8 -*-

from functools import reduce

if __name__ == '__main__':
    print(reduce(lambda a,b: ''.join([a,b]), list(map(lambda letra: letra.upper(), "Hola mundo")), ""))
