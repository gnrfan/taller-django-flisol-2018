#!/usr/bin/env python3
# -*- coding: utf8 -*-

for i in range(1, 100 + 1):
    if i % 2 != 0:
        continue
    print(i)
